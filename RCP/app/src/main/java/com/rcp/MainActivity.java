package com.rcp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import layout.tutorialFragment;

public class MainActivity extends AppCompatActivity {

    private tutorialFragment tutorial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Start the fragment tutorial
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(R.id.content_frame, tutorial = new tutorialFragment()).commit();
        }
    }
}
