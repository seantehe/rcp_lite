package layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.rcp.R;


public class tutorialFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private ImageView tutorial_imageView = null;

    private int[][] imageIds = {
            {
                    R.drawable.tutorial_form_a_1,
                    R.drawable.tutorial_form_a_2,
                    R.drawable.tutorial_form_a_3,
                    R.drawable.tutorial_form_a_4,
                    R.drawable.tutorial_form_a_5,
                    R.drawable.tutorial_form_a_6,
                    R.drawable.tutorial_form_a_7,
                    R.drawable.tutorial_form_a_8,
                    R.drawable.tutorial_form_a_9,
                    R.drawable.tutorial_form_a_10,
                    R.drawable.tutorial_form_a_11,
                    R.drawable.tutorial_form_a_12,
                    R.drawable.tutorial_form_a_13,
                    R.drawable.tutorial_form_a_14,
                    R.drawable.tutorial_form_a_15,
                    R.drawable.tutorial_form_a_16,
                    R.drawable.tutorial_form_a_17,
                    R.drawable.tutorial_form_a_18,
                    R.drawable.tutorial_form_a_19,
                    R.drawable.tutorial_form_a_20,
                    R.drawable.tutorial_form_a_21,
                    R.drawable.tutorial_form_a_22,
                    R.drawable.tutorial_form_a_23
            },
            {
                    R.drawable.tutorial_form_b_1,
                    R.drawable.tutorial_form_b_2,
                    R.drawable.tutorial_form_b_3,
                    R.drawable.tutorial_form_b_4,
                    R.drawable.tutorial_form_b_5,
                    R.drawable.tutorial_form_b_6,
                    R.drawable.tutorial_form_b_7,
                    R.drawable.tutorial_form_b_8,
                    R.drawable.tutorial_form_b_9,
                    R.drawable.tutorial_form_b_10,
                    R.drawable.tutorial_form_b_11,
                    R.drawable.tutorial_form_b_12,
                    R.drawable.tutorial_form_b_13,
                    R.drawable.tutorial_form_b_14,
                    R.drawable.tutorial_form_b_15,
                    R.drawable.tutorial_form_b_16,
                    R.drawable.tutorial_form_b_17,
                    R.drawable.tutorial_form_b_18,
                    R.drawable.tutorial_form_b_19,
                    R.drawable.tutorial_form_b_20,
                    R.drawable.tutorial_form_b_21,
                    R.drawable.tutorial_form_b_22,
                    R.drawable.tutorial_form_b_23
            }
    };

    //private OnFragmentInteractionListener mListener;

    public tutorialFragment() {
        // Required empty public constructor
    }


    public static tutorialFragment newInstance(String param1, String param2) {
        tutorialFragment fragment = new tutorialFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tutorial, container, false);
        tutorial_imageView = (ImageView) rootView.findViewById(R.id.tutorial_imageView);

        tutorial_imageView.setOnClickListener(new View.OnClickListener() {
            int number = 0;
            @Override
            public void onClick(View view) {
                if (number<23){
                    tutorial_imageView.setImageResource(imageIds[0][number]);
                    number++;
                }else{
                    Toast.makeText(getActivity(), "Termino", Toast.LENGTH_LONG).show();
                }

            }
        });



        return rootView;
    }

   /* public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

   /* public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/
}
